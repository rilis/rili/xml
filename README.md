# Rili JSON

This is module of [rili](https://gitlab.com/rilis/rili) which contain implementation of XML serializer and deserializer(parser).

**Documentation and API reference** :  [rilis.io](https://rilis.io/projects/rili/library/xml)

**[Issue tracker](https://gitlab.com/rilis/rilis/issues)**

**Project status**

[![build status](https://gitlab.com/rilis/rili/xml/badges/master/build.svg)](https://gitlab.com/rilis/rili/xml/commits/master)
[![coverage report](https://gitlab.com/rilis/rili/xml/badges/master/coverage.svg)](https://gitlab.com/rilis/rili/xml/commits/master)
