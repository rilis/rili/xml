#include <cstring>
#include <list>
#include <rili/XML.hpp>
#include <stack>
#include <string>
#include <utility>

namespace rili {
namespace {
inline static char shouldUseSingleQuote(std::string const& s) noexcept {
    for (std::size_t i = 0; i < s.size(); i++) {
        if (s[i] == '\'') {
            return false;
        } else if (s[i] == '\"') {
            return true;
        }
    }
    return false;
}

inline static bool isFormatingOnly(std::string const& s) noexcept {
    auto const formating = [](char c) { return c == 0x20 || c == 0x09 || c == 0x0D || c == 0x0A; };
    for (std::size_t i = 0; i < s.size(); i++) {
        if (!formating(s[i])) {
            return false;
        }
    }
    return true;
}

inline static std::string& stringifyImpl(std::string& result, XML const& xml) {
    switch (xml.type()) {
        case XML::Type::Text: {
            result += xml.text();
            break;
        }
        case XML::Type::Tree: {
            XML::TreeType const& tree = xml.tree();
            result += "<" + tree.name();
            if (!tree.attributes().empty()) {
                for (auto const& attribute : tree.attributes()) {
                    if (shouldUseSingleQuote(attribute.second)) {
                        result += " " + attribute.first += "=\'" + attribute.second + "\'";
                    } else {
                        result += " " + attribute.first += "=\"" + attribute.second + "\"";
                    }
                }
            }
            if (tree.nodes().empty()) {
                result += "/>";
            } else {
                result += ">";
                for (auto const& n : tree.nodes()) {
                    stringifyImpl(result, n);
                }
                result += "</" + tree.name() + ">";
            }
            break;
        }
        case XML::Type::CDATA: {
            result += "<![CDATA[" + xml.cdata() + "]]>";
            break;
        }
        case XML::Type::Comment: {
            result += "<!--" + xml.comment() + "-->";
            break;
        }
        case XML::Type::Declatation: {
            result += "<?" + xml.declaration() + "?>";
            break;
        }
        case XML::Type::Doctype: {
            result += "<!" + xml.doctype() + ">";
            break;
        }
        case XML::Type::Document: {
            XML::DocumentType const& document = xml.document();
            for (auto const& n : document.nodes()) {
                stringifyImpl(result, n);
            }
            break;
        }
        default: { throw XML::SyntaxError(0); }
    }
    return result;
}

class Handler final {
 public:
    typedef std::stack<Handler, std::list<Handler>> Stack;

    inline Handler(rili::XML& v, Handler::Stack& s) noexcept : m_value(v), m_stack(s) {}
    inline void handleCDATA(std::string&& cdata, std::size_t errorPosition);
    inline void handleComment(std::string&& comment, std::size_t errorPosition);
    inline void handleDeclaration(std::string&& declaration, std::size_t errorPosition);
    inline void handleDoctype(std::string&& doctype, std::size_t errorPosition);
    inline void handleTreeEnd(std::string&& treeName, std::size_t errorPosition);
    inline void handleText(std::string&& text, std::size_t errorPosition);
    inline void handleTreeStartEnd(std::string&& treeName, XML::TreeType::Attributes&& attributes,
                                   std::size_t errorPosition);
    inline void handleTreeStart(std::string&& treeName, XML::TreeType::Attributes&& attributes, std::size_t maxDepth,
                                std::size_t errorPosition);
    Handler() = delete;
    Handler(Handler const&) = delete;
    Handler& operator=(Handler const&) = delete;

 protected:
    rili::XML& m_value;
    Stack& m_stack;
};

inline static const char* strnstr(char const* data, char const* searched, char const* max) {
    std::size_t searchedLen = std::strlen(searched);
    while (data + searchedLen - 1 < max) {
        if (std::strncmp(data, searched, searchedLen) == 0) {
            return data;
        }
        data++;
    }
    return max;
}

inline static std::pair<std::string, rili::XML::TreeType::Attributes> fetchNameAndAttributes(
    std::string const& tree, std::size_t errorPosition) {
    std::string name;
    rili::XML::TreeType::Attributes attributes;
    char const* position = tree.c_str();
    char const* max = position + tree.size();

    auto const isSpace = [](char c) { return c == 0x20 || c == 0x09 || c == 0x0D || c == 0x0A; };

    while (position < max && !isSpace(*position)) {
        position++;
    }
    name = std::string(tree.c_str(), position);
    if (name.empty()) {
        throw rili::XML::SyntaxError(errorPosition);
    }
    while (position < max) {
        while (position < max && isSpace(*position)) {
            position++;
        }
        char const* attrNameBegin = position;
        if (attrNameBegin != max) {
            while (position < max && !isSpace(*position) && *position != '=') {
                position++;
            }
            char const* attrNameEnd = position;
            if (attrNameEnd != max) {
                while (position < max && *position != '=') {
                    position++;
                }
                char const* equalPosition = position;
                if (equalPosition != max) {
                    position++;
                    while (position < max && isSpace(*position)) {
                        position++;
                    }
                    char const* attrValueBegin = position;
                    if (attrValueBegin != max) {
                        const char quoteType = *attrValueBegin;
                        if (quoteType != '\'' && quoteType != '\"') {
                            throw rili::XML::SyntaxError(errorPosition);
                        }
                        position++;
                        while (position < max && *position != quoteType) {
                            position++;
                        }
                        char const* attrValueEnd = position;
                        position++;
                        std::string attrName(attrNameBegin, attrNameEnd);
                        if (attrName.empty()) {
                            throw rili::XML::SyntaxError(errorPosition);
                        }
                        std::string attrValue(attrValueBegin + 1, attrValueEnd);
                        attributes.push_back({std::move(attrName), std::move(attrValue)});
                    } else {
                        throw rili::XML::SyntaxError(errorPosition);
                    }
                } else {
                    throw rili::XML::SyntaxError(errorPosition);
                }
            } else {
                throw rili::XML::SyntaxError(errorPosition);
            }
        }
    }
    return {std::move(name), std::move(attributes)};
}

inline static void reader(Handler::Stack& stack, const std::string& src, size_t maxDepth) {
    char const* const startPosition = src.c_str();
    char const* position = src.c_str();
    char const* max = position + src.size();
    std::size_t errorPosition = 0;

    while (position < max) {
        errorPosition = static_cast<size_t>(position - startPosition);
        if (*position != '<') {
            char const* textEnd = position;

            while (textEnd < max && *textEnd != '<') {
                textEnd++;
            }

            std::string text(position, textEnd);
            stack.top().handleText(std::move(text), errorPosition);
            position = textEnd;

        } else if (position + 11 < max && std::strncmp(position, "<![CDATA[", 9) == 0) {
            position += 9;
            char const* cdataEnd = strnstr(position, "]]>", max);
            if (cdataEnd < max) {
                std::string cdata(position, cdataEnd);
                stack.top().handleCDATA(std::move(cdata), errorPosition);
                position = cdataEnd + 3;
            } else {
                throw XML::SyntaxError(errorPosition);
            }
        } else if (position + 6 < max && strncmp(position, "<!--", 4) == 0) {
            position += 4;
            const char* commentEnd = strnstr(position, "-->", max);
            if (commentEnd < max) {
                std::string comment(position, commentEnd);
                stack.top().handleComment(std::move(comment), errorPosition);
                position = commentEnd + 3;
            } else {
                throw XML::SyntaxError(errorPosition);
            }
        } else if (position + 3 < max && strncmp(position, "<?", 2) == 0) {
            position += 2;
            const char* declarationEnd = strnstr(position, "?>", max);
            if (declarationEnd < max) {
                std::string declaration(position, declarationEnd);
                stack.top().handleDeclaration(std::move(declaration), errorPosition);
                position = declarationEnd + 2;
            } else {
                throw XML::SyntaxError(errorPosition);
            }
        } else if (position + 2 < max && strncmp(position, "<!", 2) == 0) {
            position += 2;
            const char* doctypeStart = position;
            while (*position != '>' && *position != '[' && position < max) {
                position++;
            }
            if (position < max) {
                const char* doctypeEnd = position;
                if (*position == '[') {
                    while (*position != ']' && position < max) {
                        position++;
                    }
                    while (*position != '>' && position < max) {
                        position++;
                    }
                    if (doctypeEnd < max) {
                        doctypeEnd = position;
                    } else {
                        throw XML::SyntaxError(errorPosition);
                    }
                }
                std::string doctype(doctypeStart, doctypeEnd);
                stack.top().handleDoctype(std::move(doctype), errorPosition);
                position = doctypeEnd + 1;
            } else {
                throw XML::SyntaxError(errorPosition);
            }
        } else if (position + 2 < max && strncmp(position, "</", 2) == 0) {
            position += 2;
            const char* treeEnd = strnstr(position, ">", max);
            if (treeEnd < max) {
                std::string treeEndName(position, treeEnd);
                stack.top().handleTreeEnd(std::move(treeEndName), errorPosition);
                position = treeEnd + 1;
            } else {
                throw XML::SyntaxError(errorPosition);
            }
        } else if (*position == '<') {
            position++;
            const char* treeStartEnd = strnstr(position, ">", max);
            if (treeStartEnd < max) {
                if (*(treeStartEnd - 1) == '/') {
                    auto treeData = fetchNameAndAttributes(std::string(position, (treeStartEnd - 1)), errorPosition);
                    stack.top().handleTreeStartEnd(std::move(treeData.first), std::move(treeData.second),
                                                   errorPosition);
                } else {
                    auto treeData = fetchNameAndAttributes(std::string(position, treeStartEnd), errorPosition);
                    stack.top().handleTreeStart(std::move(treeData.first), std::move(treeData.second), maxDepth,
                                                errorPosition);
                }
                position = treeStartEnd + 1;
            } else {
                throw XML::SyntaxError(errorPosition);
            }
        } else {
            throw XML::SyntaxError(errorPosition);
        }
    }
    if (stack.size() != 1) {
        throw XML::SyntaxError(0);
    }
}

inline void Handler::handleCDATA(std::string&& cdata, std::size_t errorPosition) {
    if (m_value.type() == XML::Type::Tree) {
        auto& tree = m_value.tree();
        XML cdataNode;
        cdataNode.cdata(std::move(cdata));
        tree.nodes().emplace_back(cdataNode);
    } else {
        throw XML::SyntaxError(errorPosition);
    }
}

inline void Handler::handleComment(std::string&& comment, std::size_t errorPosition) {
    if (m_value.type() == XML::Type::Tree) {
        auto& tree = m_value.tree();
        XML commentNode;
        commentNode.comment(std::move(comment));
        tree.nodes().emplace_back(commentNode);
    } else {
        throw XML::SyntaxError(errorPosition);
    }
}

inline void Handler::handleDeclaration(std::string&& declaration, std::size_t errorPosition) {
    if (m_value.type() == XML::Type::Document) {
        auto& document = m_value.document();
        if (!document.haveRoot()) {
            XML declarationNode;
            declarationNode.declaration(std::move(declaration));
            document.nodes().emplace_back(declarationNode);
        } else {
            throw XML::SyntaxError(errorPosition);
        }
    } else {
        throw XML::SyntaxError(errorPosition);
    }
}

inline void Handler::handleDoctype(std::string&& doctype, std::size_t errorPosition) {
    if (m_value.type() == XML::Type::Document) {
        auto& document = m_value.document();
        if (!document.haveRoot()) {
            XML doctypeNode;
            doctypeNode.doctype(std::move(doctype));
            document.nodes().emplace_back(doctypeNode);
        } else {
            throw XML::SyntaxError(errorPosition);
        }
    } else {
        throw XML::SyntaxError(errorPosition);
    }
}

inline void Handler::handleText(std::string&& text, std::size_t errorPosition) {
    if (m_value.type() == XML::Type::Tree) {
        auto& tree = m_value.tree();
        XML stringNode;
        stringNode.text(std::move(text));
        tree.nodes().emplace_back(stringNode);
    } else if (m_value.type() == XML::Type::Document && isFormatingOnly(text)) {
        auto& document = m_value.document();
        XML stringNode;
        stringNode.text(std::move(text));
        document.nodes().emplace_back(stringNode);
    } else {
        throw XML::SyntaxError(errorPosition);
    }
}
inline void Handler::handleTreeStart(std::string&& treeName, XML::TreeType::Attributes&& attributes,
                                     std::size_t maxDepth, std::size_t errorPosition) {
    if (m_stack.size() < maxDepth) {
        if (m_value.type() == XML::Type::Document) {
            auto& document = m_value.document();
            if (!document.haveRoot()) {
                XML treeNode;
                treeNode.tree(XML::TreeType(treeName, attributes));
                document.nodes().emplace_back(treeNode);
                m_stack.emplace(document.nodes().back(), m_stack);
            } else {
                throw XML::SyntaxError(errorPosition);
            }
        } else if (m_value.type() == XML::Type::Tree) {
            auto& tree = m_value.tree();
            XML treeNode;
            treeNode.tree(XML::TreeType(treeName, attributes));
            tree.nodes().emplace_back(treeNode);
            m_stack.emplace(tree.nodes().back(), m_stack);
        } else {
            throw XML::SyntaxError(errorPosition);
        }
    } else {
        throw XML::SyntaxError(errorPosition);
    }
}

inline void Handler::handleTreeEnd(std::string&& treeName, std::size_t errorPosition) {
    if (m_value.type() == XML::Type::Tree && m_stack.size() > 1 && m_value.tree().name() == treeName) {
        m_stack.pop();
    } else {
        throw XML::SyntaxError(errorPosition);
    }
}

inline void Handler::handleTreeStartEnd(std::string&& treeName, XML::TreeType::Attributes&& attributes,
                                        std::size_t errorPosition) {
    if (m_value.type() == XML::Type::Document) {
        auto& document = m_value.document();
        if (!document.haveRoot()) {
            XML treeNode;
            treeNode.tree(XML::TreeType(treeName, attributes));
            document.nodes().emplace_back(treeNode);
        } else {
            throw XML::SyntaxError(errorPosition);
        }
    } else if (m_value.type() == XML::Type::Tree) {
        auto& tree = m_value.tree();
        XML treeNode;
        treeNode.tree(XML::TreeType(treeName, attributes));
        tree.nodes().emplace_back(treeNode);
    } else {
        throw XML::SyntaxError(errorPosition);
    }
}
}  // namespace
XML::SyntaxError::SyntaxError(size_t position) noexcept
    : std::exception(),
      m_position(position),
      m_what("rili::XML::SyntaxError at " + std::to_string(m_position)) {}

const char* XML::SyntaxError::what() const noexcept { return m_what.c_str(); }

size_t XML::SyntaxError::position() const noexcept { return m_position; }

void XML::parse(const std::string& json, size_t maxDepth) {
    m_storage.set<DocumentType>(DocumentType{});  // NOLINT
    m_type = Type::Document;

    Handler::Stack stack;
    stack.emplace(*this, stack);
    reader(stack, json, maxDepth);
}

std::string XML::stringify() const noexcept {
    std::string result;
    return stringifyImpl(result, *this);
}
}  // namespace rili
