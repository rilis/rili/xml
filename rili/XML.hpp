#pragma once
/** @file */

#include <exception>
#include <list>
#include <rili/Variant.hpp>
#include <string>
#include <utility>

namespace rili {
/**
 * @brief The XML class represent generic XML node.
 *
 * @note It can be tree(`<`` something ``>`), text, doctype( `<``!`` something ``!``>` ),
 * comment( `<``!``-``-`` something ``-``-``>` ), cdata(`<``!``[``CDATA``[`` something ``]``]``>`),
 * declaration(`<``?`` something ``?``>`)
 *
 * @warning inner parameters of doctype and declaration nodes is not fully  parsed -
 * instead data between markers is stored as single string.
 */
class XML final {
 public:
    /**
     * @brief The SyntaxError class is used to communicate about invalid XML synthax
     */
    class SyntaxError final : public std::exception {
     public:
        /**
         * @brief SyntaxError used to construct exception with known position of error in original XML string
         * @param position aproximate place where error was found
         */
        explicit SyntaxError(size_t position) noexcept;
        /**
         * @brief what is used to retrieve exception message
         * @return exception message
         */
        const char* what() const noexcept override;

        /**
         * @brief position  used to retrieve error position
         * @return postion
         */
        size_t position() const noexcept;

        virtual ~SyntaxError() = default;

        /* @cond INTERNAL */
        SyntaxError(SyntaxError const& other) = default;
        SyntaxError& operator=(SyntaxError const& other) = default;
        /* @endcond INTERNAL */

     private:
        SyntaxError() = delete;
        size_t m_position;
        std::string m_what;
    };

    /**
     * @brief The TreeType class used to store tree type nodes.
     *
     * @note Tree type nodes are in form: `<``node``>``...``<``/``node``>` or `<``node``/``>`
     */
    class TreeType final {
     public:
        /**
         * @brief Nodes type in which child nodes are stored
         */
        typedef std::list<XML> Nodes;

        /**
         * @brief Attribute type in whch attribure is stored. `Attribute::first` is attribute name, `Attribute::second`
         * is attribute value
         */
        typedef std::pair<std::string, std::string> Attribute;
        /**
         * @brief Attributes type in which current node attrivutes are stored
         */
        typedef std::list<Attribute> Attributes;

     public:
        /**
         * @brief name return tag name( for "`<``node ``/``>`" it should be equal to `node` )
         * @return tag name
         */
        inline std::string& name() noexcept { return m_name; }
        /**
         * @brief name return tag name( for "`<``node ``/``>`" it should be equal to `node` )
         * @return tag name
         */
        inline std::string const& name() const noexcept { return m_name; }

        /**
         * @brief nodes used to get child nodes
         * @return child nodes
         */
        inline Nodes& nodes() noexcept { return m_nodes; }
        /**
         * @brief nodes used to get child nodes
         * @return child nodes
         */
        inline Nodes const& nodes() const noexcept { return m_nodes; }

        /**
         * @brief attributes used to get current tag attributes
         * @return current tag attributes
         */
        inline Attributes& attributes() noexcept { return m_attributes; }
        /**
         * @brief attributes used to get current tag attributes
         * @return current tag attributes
         */
        inline Attributes const& attributes() const noexcept { return m_attributes; }

     public:
        TreeType() = delete;
        /**
         * @brief TreeType construct tree node with given name, without attributes nor subnodes
         * @param name name of tree node tag to construct
         */
        explicit TreeType(std::string const& name) : TreeType(name, {}, {}) {}
        /**
         * @brief TreeType construct tree node with given name and subnodes but without attributes
         * @param name name of tree node tag to construct
         * @param nodes list of subnodes
         */
        TreeType(std::string const& name, Nodes const& nodes) : TreeType(name, nodes, {}) {}
        /**
         * @brief TreeType construct tree node with given name and attributes but without subnodes
         * @param name name of tree node tag to construct
         * @param attributes list of attributes
         */
        TreeType(std::string const& name, Attributes const& attributes) : TreeType(name, {}, attributes) {}
        /**
         * @brief TreeType
         * @param name name of tree node tag to construct
         * @param nodes list of subnodes
         * @param attributes list of attributes
         */
        TreeType(std::string const& name, Nodes const& nodes, Attributes const& attributes)
            : m_name(name), m_nodes(nodes), m_attributes(attributes) {}

     private:
        std::string m_name;
        Nodes m_nodes;
        Attributes m_attributes;
    };

    /**
     * @brief The DocumentType class is used to store document - it do not have own markup - it's just root element for
     * all other elements in XML document
     */
    class DocumentType final {
     public:
        /**
         * @brief Nodes type in which child nodes are stored
         */
        typedef std::list<XML> Nodes;

     public:
        /**
         * @brief nodes used to get child nodes
         * @return child nodes
         */
        inline Nodes& nodes() noexcept { return m_nodes; }
        /**
         * @brief nodes used to get child nodes
         * @return child nodes
         */
        inline Nodes const& nodes() const noexcept { return m_nodes; }

        /**
         * @brief haveRoot indicate if document have root element (tree node)
         * @return true if have tree node, false otherwise
         */
        inline bool haveRoot() const noexcept {
            return std::find_if(m_nodes.begin(), m_nodes.end(),
                                [](XML const& n) { return n.type() == XML::Type::Tree; }) != m_nodes.end();
        }

     public:
        /**
         * @brief DocumentType construct document node with without subnodes
         */
        DocumentType() : m_nodes() {}
        /**
         * @brief DocumentType construct document node with with subnodes
         * @param nodes list of subnodes
         */
        explicit DocumentType(Nodes const& nodes) : m_nodes(nodes) {}

     private:
        Nodes m_nodes;
    };
    /**
     * @brief TextType type in which text nodes are stored
     */
    typedef std::string TextType;
    /**
     * @brief CDATAType type in which cdata nodes are stored
     */
    typedef std::string CDATAType;
    /**
     * @brief CommentType type in which comments are stored
     */
    typedef std::string CommentType;
    /**
     * @brief DoctypeType type in which doctype node is stored
     */
    typedef std::string DoctypeType;
    /**
     * @brief DeclarationType type in which declaration nodes are stored
     */
    typedef std::string DeclarationType;

    /**
     * @brief The Type enum is used to check which
     */
    enum class Type { Document, Tree, Text, CDATA, Comment, Doctype, Declatation };

    /**
     * @brief type used to check type of node
     * @return
     */
    inline Type type() const noexcept { return m_type; }
    /**
     * @brief tree used to get node as tree node
     * @return
     */
    inline TreeType& tree() {
        if (m_type != Type::Tree) {
            throw std::bad_cast();
        }
        return m_storage.get<TreeType>();
    }

    /**
     * @brief tree used to get node as tree node
     * @return
     */
    inline TreeType const& tree() const {
        if (m_type != Type::Tree) {
            throw std::bad_cast();
        }
        return m_storage.get<TreeType>();
    }

    /**
     * @brief tree used to set node as tree node
     * @param t
     */
    inline void tree(TreeType const& t) {
        m_type = Type::Tree;
        m_storage.set<TreeType>(t);
    }

    /**
     * @brief text used to get node as text node
     * @return
     */
    inline TextType& text() {
        if (m_type != Type::Text) {
            throw std::bad_cast();
        }
        return m_storage.get<TextType>();
    }

    /**
     * @brief text used to get node as text node
     * @return
     */
    inline TextType const& text() const {
        if (m_type != Type::Text) {
            throw std::bad_cast();
        }
        return m_storage.get<TextType>();
    }

    /**
     * @brief text used to set node as text node
     * @param t
     */
    inline void text(TextType const& t) {
        m_type = Type::Text;
        m_storage.set<TextType>(t);
    }

    /**
     * @brief cdata used to get node as cdata node
     * @return
     */
    inline CDATAType& cdata() {
        if (m_type != Type::CDATA) {
            throw std::bad_cast();
        }
        return m_storage.get<CDATAType>();
    }

    /**
     * @brief cdata used to get node as cdata node
     * @return
     */
    inline CDATAType const& cdata() const {
        if (m_type != Type::CDATA) {
            throw std::bad_cast();
        }
        return m_storage.get<CDATAType>();
    }

    /**
     * @brief cdata used to set node as cdata node
     * @param t
     */
    inline void cdata(CDATAType const& t) {
        m_type = Type::CDATA;
        m_storage.set<CDATAType>(t);
    }

    /**
     * @brief comment used to get node as comment node
     * @return
     */
    inline CommentType& comment() {
        if (m_type != Type::Comment) {
            throw std::bad_cast();
        }
        return m_storage.get<CommentType>();
    }

    /**
     * @brief comment used to get node as comment node
     * @return
     */
    inline CommentType const& comment() const {
        if (m_type != Type::Comment) {
            throw std::bad_cast();
        }
        return m_storage.get<CommentType>();
    }

    /**
     * @brief comment used to set node as comment node
     * @param t
     */
    inline void comment(CommentType const& t) {
        m_type = Type::Comment;
        m_storage.set<CommentType>(t);
    }

    /**
     * @brief declaration used to get node as declaration node
     * @return
     */
    inline DeclarationType& declaration() {
        if (m_type != Type::Declatation) {
            throw std::bad_cast();
        }
        return m_storage.get<DeclarationType>();
    }

    /**
     * @brief declaration used to get node as declaration node
     * @return
     */
    inline DeclarationType const& declaration() const {
        if (m_type != Type::Declatation) {
            throw std::bad_cast();
        }
        return m_storage.get<DeclarationType>();
    }

    /**
     * @brief declaration used to set node as declaration node
     * @param t
     */
    inline void declaration(DeclarationType const& t) {
        m_type = Type::Declatation;
        m_storage.set<DeclarationType>(t);
    }

    /**
     * @brief doctype used to get node as doctype node
     * @return
     */
    inline DoctypeType& doctype() {
        if (m_type != Type::Doctype) {
            throw std::bad_cast();
        }
        return m_storage.get<DoctypeType>();
    }

    /**
     * @brief doctype used to get node as doctype node
     * @return
     */
    inline DoctypeType const& doctype() const {
        if (m_type != Type::Doctype) {
            throw std::bad_cast();
        }
        return m_storage.get<DoctypeType>();
    }

    /**
     * @brief doctype used to set node as doctype node
     * @param t
     */
    inline void doctype(DoctypeType const& t) {
        m_type = Type::Doctype;
        m_storage.set<DoctypeType>(t);
    }

    /**
     * @brief document used to get node as document node
     * @return
     */
    inline DocumentType& document() {
        if (m_type != Type::Document) {
            throw std::bad_cast();
        }
        return m_storage.get<DocumentType>();
    }

    /**
     * @brief document used to get node as document node
     * @return
     */
    inline DocumentType const& document() const {
        if (m_type != Type::Document) {
            throw std::bad_cast();
        }
        return m_storage.get<DocumentType>();
    }

    /**
     * @brief document used to set node as document node
     * @param t
     */
    inline void document(DocumentType const& t) {
        m_type = Type::Document;
        m_storage.set<DocumentType>(t);
    }

    /**
     * @brief XML construct documet type empty node
     */
    inline XML() noexcept : m_type(Type::Document), m_storage() {
        DocumentType d;
        m_storage.set<DocumentType>(d);  // NOLINT
    }
    /**
     * @brief XML used to construct nodes tree based on given xml string
     * @param xml data to parse
     * @param maxDepth maximal allowed nasted elements depth
     */
    inline XML(std::string const& xml, size_t maxDepth = 100) : XML() { parse(xml, maxDepth); }
    /**
     * @brief parse used to parse nodes tree based on given xml string on current xml object
     * @param xml data to parse
     * @param maxDepth maximal allowed nasted elements depth
     */
    void parse(const std::string& xml, size_t maxDepth = 100);
    /**
     * @brief stringify convert XML objects tree into string document representation
     * @return stringified xml document
     */
    std::string stringify() const noexcept;

 private:
    Type m_type;
    Variant<TextType, CDATAType, CommentType, DoctypeType, DeclarationType, TreeType, DocumentType> m_storage;
};
}  // namespace rili
