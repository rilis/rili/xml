#include <rili/Test.hpp>
#include <rili/XML.hpp>
#include <string>

TEST(Basic, EmptyDocument) {
    std::string doc = "";
    std::string expected = "";
    rili::XML document;
    document.parse(doc);

    EXPECT_EQ(expected, document.stringify());
}

TEST(Basic, DocumentWithSimpleDoctype) {
    std::string doc = "<!DOCTYPE note SYSTEM \"Note.dtd\">";
    std::string expected = doc;
    rili::XML document;
    document.parse(doc);

    EXPECT_EQ(expected, document.stringify());
}

TEST(Basic, DocumentWithWhitespaces) {
    std::string doc = "\n\t\n    \n<!DOCTYPE note SYSTEM \"Note.dtd\">\n\r\n";
    std::string expected = doc;
    rili::XML document;
    document.parse(doc);

    EXPECT_EQ(expected, document.stringify());
}

TEST(Basic, DocumentWithComplexDoctype) {
    std::string doc =
        "<!DOCTYPE note\n"
        "[\n"
        "<!ELEMENT note (to,from,heading,body)>\n"
        "<!ELEMENT to (#PCDATA)>\n"
        "<!ELEMENT from (#PCDATA)>\n"
        "<!ELEMENT heading (#PCDATA)>\n"
        "<!ELEMENT body (#PCDATA)>\n"
        "]>";
    std::string expected = doc;
    rili::XML document;
    document.parse(doc);

    EXPECT_EQ(expected, document.stringify());
}

TEST(Basic, DocumentWithDeclaration) {
    std::string doc = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    std::string expected = doc;
    rili::XML document;
    document.parse(doc);

    EXPECT_EQ(expected, document.stringify());
}

TEST(Basic, DocumentWithBeginEndSingleNode) {
    std::string doc = "<node/>";
    std::string expected = doc;
    rili::XML document;
    document.parse(doc);

    EXPECT_EQ(expected, document.stringify());
}

TEST(Basic, DocumentWithSingleEmptyTree) {
    std::string doc = "<node></node>";
    std::string expected = "<node/>";
    rili::XML document;
    document.parse(doc);

    EXPECT_EQ(expected, document.stringify());
}

TEST(Basic, DocumentWithSingleNotEmptyTree) {
    std::string doc = "<node>something</node>";
    std::string expected = doc;
    rili::XML document;
    document.parse(doc);

    EXPECT_EQ(expected, document.stringify());
}

TEST(Basic, DocumentWithComment) {
    std::string doc = "<node><!-- This is a -- comment --></node>";
    std::string expected = doc;
    rili::XML document;
    document.parse(doc);

    EXPECT_EQ(expected, document.stringify());
}

TEST(Basic, DocumentWithCdata) {
    std::string doc = "<node><![CDATA[some data <some pseudo xml>]]></node>";
    std::string expected = doc;
    rili::XML document;
    document.parse(doc);

    EXPECT_EQ(expected, document.stringify());
}

TEST(Basic, DocumentWithTreeAndAttributes) {
    std::string doc =
        "<node><node attr = 'val'/><node attr = 'va\"l'/><node attr = \"val\"/><node attr = \"va'l\"/></node>";
    std::string expected =
        "<node><node attr=\"val\"/><node attr='va\"l'/><node attr=\"val\"/><node attr=\"va'l\"/></node>";
    rili::XML document;
    document.parse(doc);

    EXPECT_EQ(expected, document.stringify());
}

TEST(Basic, DocumentWithNastedTrees) {
    std::string doc = "<node><node><node><node>something</node></node></node></node>";
    std::string expected = "<node><node><node><node>something</node></node></node></node>";
    rili::XML document;
    document.parse(doc);

    EXPECT_EQ(expected, document.stringify());
}

TEST(Basic, ComplexDocument) {
    std::string doc =
        "<?xml version=\"1.0\"?>\n"
        "<?xml-stylesheet href=\"catalog.xsl\" type=\"text/xsl\"?>\n"
        "<!DOCTYPE catalog SYSTEM \"catalog.dtd\">\n"
        "<catalog>\n"
        "   <product description=\"Cardigan Sweater\" product_image=\"cardigan.jpg\">\n"
        "      <catalog_item gender=\"Men\'s\">\n"
        "         <item_number>QWZ5671</item_number>\n"
        "         <price>39.95</price>\n"
        "         <size description=\"Medium\">\n"
        "            <color_swatch image=\"red_cardigan.jpg\">Red</color_swatch>\n"
        "            <color_swatch image=\"burgundy_cardigan.jpg\">Burgundy</color_swatch>\n"
        "         </size>\n"
        "         <size description=\"Large\">\n"
        "            <color_swatch image=\"red_cardigan.jpg\">Red</color_swatch>\n"
        "            <color_swatch image=\"burgundy_cardigan.jpg\">Burgundy</color_swatch>\n"
        "         </size>\n"
        "      </catalog_item>\n"
        "      <catalog_item gender=\"Women\'s\">\n"
        "         <item_number>RRX9856</item_number>\n"
        "         <price>42.50</price>\n"
        "         <size description=\"Small\">\n"
        "            <color_swatch image=\"red_cardigan.jpg\">Red</color_swatch>\n"
        "            <color_swatch image=\"navy_cardigan.jpg\">Navy</color_swatch>\n"
        "            <color_swatch image=\"burgundy_cardigan.jpg\">Burgundy</color_swatch>\n"
        "         </size>\n"
        "         <size description=\"Medium\">\n"
        "            <color_swatch image=\"red_cardigan.jpg\">Red</color_swatch>\n"
        "            <color_swatch image=\"navy_cardigan.jpg\">Navy</color_swatch>\n"
        "            <color_swatch image=\"burgundy_cardigan.jpg\">Burgundy</color_swatch>\n"
        "            <color_swatch image=\"black_cardigan.jpg\">Black</color_swatch>\n"
        "         </size>\n"
        "         <size description=\"Large\">\n"
        "            <color_swatch image=\"navy_cardigan.jpg\">Navy</color_swatch>\n"
        "            <color_swatch image=\"black_cardigan.jpg\">Black</color_swatch>\n"
        "         </size>\n"
        "         <size description=\"Extra Large\">\n"
        "            <color_swatch image=\"burgundy_cardigan.jpg\">Burgundy</color_swatch>\n"
        "            <color_swatch image=\"black_cardigan.jpg\">Black</color_swatch>\n"
        "         </size>\n"
        "      </catalog_item>\n"
        "   </product>\n"
        "</catalog>";
    std::string expected = doc;
    rili::XML document;
    document.parse(doc);

    EXPECT_EQ(expected, document.stringify());
}
