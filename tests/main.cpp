#include <iostream>
#include <rili/Test.hpp>
#include <rili/XML.hpp>
#include <string>

int main(int, char**) { return rili::test::runner::run() ? 0 : 1; }
